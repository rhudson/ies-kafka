# Kafka API examples in Scala

Scala versions of the Kafka examples are in ies.kafka.examples

Original Java examples can be found here:
https://github.com/apache/kafka/tree/0.8.1/examples


## Kafka Setup

These examples assume that Kafka is installed and running on port 9092.

	# Kafka is easy to install on OSX using brew
	# Perhaps this should be a Docker container instead?
	brew install kafka
	
	# Run the server
	kafka-server-start.sh /usr/local/etc/kafka/server.properties


Topics should be created before running the code.
e.g.

	kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic1
	kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic2
	kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic3

To purge a topic, first delete it then create it again.

	kafka-topics.sh --delete --zookeeper localhost:2181 --topic topic1
	kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic1


## Running the code

	sbt clean compile

	# Run from sbt
	sbt "runMain ies.kafka.examples.KafkaConsumerProducerDemo"
	sbt "runMain ies.kafka.examples.SimpleConsumerDemo"

	# Or build an uber jar using the one-jar plugin
	sbt one-jar
	java -jar ./target/scala-2.10/ies_kafka_2.10-1.0-one-jar.jar


## Inspecting network traffic

A dissector for the Kafka protocol has been included in Wireshark since version 1.11.3.
 
http://www.wireshark.org/docs/relnotes/wireshark-1.11.3.html#_new_protocol_support

This can be useful for learning the protocol or getting a feel for what the API is doing.


1 - Run Wireshark
1 - Set the Kafka broker port in the preferences here: Wireshark->Preferences->Protocols->Kafka
1 - Restart Wireshark
1 - Select interface (localhost) and set capture filter. e.g. - tcp port 9092
1 - Set display filter to "kafka"
1 - Run Kafka producer/consumer to see traffic

 