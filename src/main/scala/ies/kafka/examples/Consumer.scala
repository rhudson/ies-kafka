package ies.kafka.examples

import java.util.Properties
import kafka.consumer.ConsumerConfig
import java.util.HashMap

/**
 * Converted from kafka.examples.Consumer found here:
 * https://github.com/apache/kafka/tree/0.8.1/examples
 */
class Consumer(topic: String) extends Thread with KafkaProperties {

  def createConsumerConfig():ConsumerConfig = {
    
    val props = new Properties()
    props.put("zookeeper.connect", zkConnect);
    props.put("group.id", groupId);
    props.put("zookeeper.session.timeout.ms", "400");
    props.put("zookeeper.sync.time.ms", "200");
    props.put("auto.commit.interval.ms", "1000");

    return new ConsumerConfig(props)
  }
  
  val consumer = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig())
  
  override def run() = {
    
    val topicCountMap = new HashMap[String, Integer]()
    topicCountMap.put(topic, new Integer(1))
    
    val consumerMap = consumer.createMessageStreams(topicCountMap)
    val stream =  consumerMap.get(topic).get(0)
    val it = stream.iterator()
    while (it.hasNext()) {
      println(new String(it.next().message()))
    }
  }

}