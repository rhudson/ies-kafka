package ies.kafka.examples

/**
 * Converted from kafka.examples.KafkaConsumerProducerDemo found here:
 * https://github.com/apache/kafka/tree/0.8.1/examples
 */
object KafkaConsumerProducerDemo extends KafkaProperties {
  
  def main(args: Array[String]): Unit = {
    val producerThread = new Producer(topicName)
    producerThread.start()

    val consumerThread = new Consumer(topicName)
    consumerThread.start()
  }
  
}