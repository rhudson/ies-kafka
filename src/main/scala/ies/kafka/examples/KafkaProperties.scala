package ies.kafka.examples

/**
 * Converted from kafka.examples.KafkaProperties found here:
 * https://github.com/apache/kafka/tree/0.8.1/examples
 */
trait KafkaProperties {
  
  val zkConnect = "127.0.0.1:2181"
  val groupId = "group1";
  val topicName = "topic1";
  val kafkaServerURL = "localhost";
  val kafkaServerPort = 9092;
  val kafkaProducerBufferSize = 64*1024;
  val connectionTimeOut = 100000;
  val reconnectInterval = 10000;
  val topic2 = "topic2";
  val topic3 = "topic3";
  val clientId = "SimpleConsumerDemoClient";
  
}