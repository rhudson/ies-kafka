package ies.kafka.examples

import kafka.api.FetchRequestBuilder
import kafka.javaapi.message.ByteBufferMessageSet
import scala.collection.JavaConversions._
import java.util.HashMap
import java.util.ArrayList

object SimpleConsumerDemo {

  def main(args: Array[String]): Unit = {
    val consumerThread = new SimpleConsumerDemo()
    consumerThread.start()
  }

}

/**
 * Converted from kafka.examples.SimpleConsumerDemo found here:
 * https://github.com/apache/kafka/tree/0.8.1/examples
 */
class SimpleConsumerDemo extends Thread with KafkaProperties {

  private def printMessages(messageSet: ByteBufferMessageSet) = {
    val iterator = messageSet.iterator
    while (iterator.hasNext()) {
      val messageAndOffset = iterator.next()
      val payload = messageAndOffset.message.payload
      val bytes = new Array[Byte](payload.limit)
      payload.get(bytes)
      println(new String(bytes, "UTF-8"))
    }
  }

  private def generateData() = {
    val producer2 = new Producer(topic2)
    producer2.start()
    val producer3 = new Producer(topic3)
    producer3.start()
    try {
      Thread.sleep(1000)
    } catch {
      case e: InterruptedException => {
        e.printStackTrace()
      }
    }
  }

  override def run() = {

    generateData()

    val simpleConsumer = new kafka.javaapi.consumer.SimpleConsumer(kafkaServerURL,
      kafkaServerPort,
      connectionTimeOut,
      kafkaProducerBufferSize,
      clientId)

    println("Testing single fetch")
    // The fetch size is a byte count, not a message count.  Setting to 100 bytes
    // will probably only fetch a single message from Kafka.
    var req = new FetchRequestBuilder()
      .clientId(clientId)
      .addFetch(topic2, 0, 0L, 100)
      .build()
    var fetchResponse = simpleConsumer.fetch(req)
    printMessages(fetchResponse.messageSet(topic2, 0))

    println("Testing single multi-fetch")
    val topicMap = new HashMap[String, java.util.List[Integer]]() {
      {
        put(topic2, new ArrayList[Integer]() { { add(0) } })
        put(topic3, new ArrayList[Integer]() { { add(0) } })
      }
    }
    // fetch 1000 bytes from topic2 so that we get more than one message
    req = new FetchRequestBuilder()
      .clientId(clientId)
      .addFetch(topic2, 0, 0L, 1000)
      .addFetch(topic3, 0, 0L, 100)
      .build()
    fetchResponse = simpleConsumer.fetch(req)
    var fetchReq = 0

    val iterator = topicMap.entrySet().iterator

    while (iterator.hasNext()) {
      val entry = iterator.next()
      val topic = entry.getKey()

      val valueIter = entry.getValue().iterator
      while (valueIter.hasNext()) {
        val offset = valueIter.next()
        fetchReq = fetchReq + 1
        println("Response from fetch request no: " + fetchReq)
        printMessages(fetchResponse.messageSet(topic, offset))
      }

    }

  }

}