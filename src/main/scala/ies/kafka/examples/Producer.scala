package ies.kafka.examples

import kafka.producer.ProducerConfig
import java.util.Properties
import kafka.producer.KeyedMessage

/**
 * Converted from kafka.examples.Producer found here:
 * https://github.com/apache/kafka/tree/0.8.1/examples
 */
class Producer(topic: String) extends Thread {

  val messageLimit = 1000
  
  val keyCount = 3
  val keyList = 1.to(keyCount).map(num => { "key" + num })
  
  // Initialize the Map of keys with count values
  val keyMap = new scala.collection.mutable.HashMap[String, Int]()
  keyList.foreach(key => {keyMap.put(key, 0)})
  
  val props = new Properties()
  props.put("serializer.class", "kafka.serializer.StringEncoder")
  props.put("metadata.broker.list", "localhost:9092")
 
  val producer = new kafka.javaapi.producer.Producer[String, String](new ProducerConfig(props))
  
  override def run() = {
    var messageNo = 0

    while(messageNo < messageLimit)
    {
      messageNo = messageNo + 1;
      
      // Get a random partition key to write to
      val key = keyList(scala.util.Random.nextInt (keyCount))
      val count = keyMap.get(key).get
      
      // track how many messages go to each partition
      keyMap.put(key, count + 1)
      
      val messageStr = "Message_" + key + "_" + f"${count}%08d"
      producer.send(new KeyedMessage[String, String](topic, key, messageStr))
    }
    
    println(keyMap)
  }
  
}