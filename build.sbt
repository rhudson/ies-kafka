name := """ies_kafka"""

version := "1.0"

scalaVersion := "2.10.2"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

libraryDependencies ++= Seq(
//	"scala-kafka" % "scala-kafka" % "0.1.0.0",
	"org.apache.kafka" % "kafka_2.10" % "0.8.1"
	    exclude("javax.jms", "jms")
	    exclude("com.sun.jdmk", "jmxtools")
	    exclude("com.sun.jmx", "jmxri"),
	"commons-lang" % "commons-lang" % "2.6",
	"junit" % "junit" % "4.11" % "test",
	"org.scalatest" %% "scalatest" % "1.9.1" % "test"
)

