
object playground {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val count = new java.util.concurrent.atomic.AtomicInteger(3)
                                                  //> count  : java.util.concurrent.atomic.AtomicInteger = 3
  count.getAndIncrement()                         //> res0: Int = 3
  count.getAndIncrement()                         //> res1: Int = 4

	val map = Map("key1" -> count)            //> map  : scala.collection.immutable.Map[String,java.util.concurrent.atomic.Ato
                                                  //| micInteger] = Map(key1 -> 5)
  map.get("key1")                                 //> res2: Option[java.util.concurrent.atomic.AtomicInteger] = Some(5)
  count.getAndIncrement()                         //> res3: Int = 5
  map.get("key1")                                 //> res4: Option[java.util.concurrent.atomic.AtomicInteger] = Some(6)
  
  val map2 = new scala.collection.mutable.HashMap[String, Int]()
                                                  //> map2  : scala.collection.mutable.HashMap[String,Int] = Map()
	val key1 = "key1"                         //> key1  : String = key1
  map2.put(key1, 1)                               //> res5: Option[Int] = None
  map2.get(key1)                                  //> res6: Option[Int] = Some(1)
  
  map2.put(key1, map2.get(key1).get + 1)          //> res7: Option[Int] = Some(1)
  map2.put(key1, map2.get(key1).get + 1)          //> res8: Option[Int] = Some(2)
  map2.put(key1, map2.get(key1).get + 1)          //> res9: Option[Int] = Some(3)
  map2.get(key1)                                  //> res10: Option[Int] = Some(4)

	f"${map2.get(key1).get}%03d-yeah"         //> res11: String = 004-yeah

	val countVal = map2.get(key1).get         //> countVal  : Int = 4
	"%05d".format(countVal)                   //> res12: String = 00004
	f"${countVal}%05d"                        //> res13: String = 00004
	f"${12}%05d"                              //> res14: String = 00012
	f"${123456}%05d"                          //> res15: String = 123456

	val keys = List("key1", "key2")           //> keys  : List[String] = List(key1, key2)
	val keyCount = 11                         //> keyCount  : Int = 11
	
	val keyList = 1.to(keyCount).map(num => { "key" + num })
                                                  //> keyList  : scala.collection.immutable.IndexedSeq[String] = Vector(key1, key2
                                                  //| , key3, key4, key5, key6, key7, key8, key9, key10, key11)

  val keyMap = new scala.collection.mutable.HashMap[String, Int]()
                                                  //> keyMap  : scala.collection.mutable.HashMap[String,Int] = Map()
	keyList.foreach(key => {keyMap.put(key, 0)})
	keyMap                                    //> res16: scala.collection.mutable.HashMap[String,Int] = Map(key6 -> 0, key9 ->
                                                  //|  0, key8 -> 0, key2 -> 0, key5 -> 0, key11 -> 0, key4 -> 0, key7 -> 0, key1 
                                                  //| -> 0, key10 -> 0, key3 -> 0)
	keyList(4)                                //> res17: String = key5
	keyList(scala.util.Random.nextInt (keyCount))
                                                  //> res18: String = key5
	keyList(scala.util.Random.nextInt (keyCount))
                                                  //> res19: String = key7
	keyList(scala.util.Random.nextInt (keyCount))
                                                  //> res20: String = key9
	keyList(scala.util.Random.nextInt (keyCount))
                                                  //> res21: String = key1
	keyList(scala.util.Random.nextInt (keyCount))
                                                  //> res22: String = key4
}